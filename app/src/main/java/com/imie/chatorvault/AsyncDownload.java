package com.imie.chatorvault;

import android.net.Uri;
import android.os.AsyncTask;
import com.imie.chatorvault.API.Message;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by quentin for Chat on 19/12/2017.
 */

public class AsyncDownload extends AsyncTask<java.net.URL,Integer,java.lang.String> {
    private MessageAdapter messageAdapter;

    public void setMessageAdapter(MessageAdapter messageAdapter) {
        this.messageAdapter = messageAdapter;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(java.lang.String string) {
        super.onPostExecute(string);
        Gson gson = new Gson();
        ArrayList<Message> listMessage = gson.fromJson(
                string,
                new TypeToken<List<Message>>(){}.getType());

        messageAdapter.addMessages(listMessage);

    }

    @Override
    protected java.lang.String doInBackground(java.net.URL... urls) {
        try{
            return getResponseFromHttpUrl(urls[0]);
        }catch (IOException e){
            e.printStackTrace();
            return "error";
        }

    }



    public static java.lang.String getResponseFromHttpUrl(java.net.URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }
}
