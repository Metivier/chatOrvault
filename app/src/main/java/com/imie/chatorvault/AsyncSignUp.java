package com.imie.chatorvault;

import android.app.backup.BackupDataInputStream;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imie.chatorvault.API.Auteur;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by quentin for Chat on 19/12/2017.
 */

public class AsyncSignUp extends AsyncTask<URL,Integer,String> {
    String nom;
    String mdp;
    PostExecuteSignUp postExecuteSignUp;
    public AsyncSignUp(String nom, String mdp,PostExecuteSignUp pesu) {
        this.nom = nom;
        this.mdp = mdp;
        this.postExecuteSignUp = pesu;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String string) {
        super.onPostExecute(string);
    }

    @Override
    protected String doInBackground(URL... urls) {

        //PARAMETRES
        Auteur auteur = new Auteur(nom,mdp);
        String retour = null;

        OutputStream out = null;
        try {
            HttpURLConnection conn = (HttpURLConnection) urls[0].openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept","application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);

            Log.i("JSON", new Gson().toJson(auteur));
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());

            //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
            os.writeBytes(new Gson().toJson(auteur));
            os.close();
            os.flush();
            try {
                InputStream in = conn.getInputStream();

                Scanner scanner = new Scanner(in);
                scanner.useDelimiter("\\A");

                boolean hasInput = scanner.hasNext();
                if (hasInput) {
                    Gson gson = new Gson();
                    Auteur auteurRetour= gson.fromJson(scanner.next(),Auteur.class);
                    if (conn.getResponseCode() == 201)
                        postExecuteSignUp.onPostExecuteSignUp(auteurRetour);
                } else {
                    return null;
                }
            } finally {
                conn.disconnect();
            }

            Log.i("STATUS", String.valueOf(conn.getResponseCode()));
            Log.i("MSG" , conn.getResponseMessage());

            conn.disconnect();
            return retour;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "";
        }
    }

    public interface PostExecuteSignUp {
        void  onPostExecuteSignUp(Auteur auteur);
    }
}
