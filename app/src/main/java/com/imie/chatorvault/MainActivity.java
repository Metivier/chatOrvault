package com.imie.chatorvault;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recycleViewMessage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText editTextPost = findViewById(R.id.editTextPost);
                editTextPost.setVisibility(View.VISIBLE);
                fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                        R.drawable.ic_send_black_24dp));
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AsyncPostMessage asyncPostMessage = new AsyncPostMessage(
                                editTextPost.getText().toString(),
                                new AsyncPostMessage.PostExecuteMessage() {
                                    @Override
                                    public void onPostExecuteMessage(String contenu) {

                                    }
                                });
                        try{
                            asyncPostMessage.execute(buildURI());
                        }catch(MalformedURLException e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        //try{
        //    AsyncDownload async =new AsyncDownload();
        //    //async.setMessageAdapter(mAdapter);
        //    async.execute(buildURI());
        //}catch (IOException e){
        //    e.printStackTrace();
        //}

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public java.net.URL buildURI() throws MalformedURLException {
        Uri constructionURI = Uri.parse(getString(R.string.url_serveur)).buildUpon()
                .appendEncodedPath(getString(R.string.path_message).concat("/"))
                .build();
        return new java.net.URL(constructionURI.toString());
    }



}
