package com.imie.chatorvault;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imie.chatorvault.API.Auteur;
import com.imie.chatorvault.API.Message;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by quentin for Chat on 19/12/2017.
 */

public class AsyncLogin extends AsyncTask<URL,Integer,String> {
    private  PostExecuteList pel;

    public AsyncLogin(PostExecuteList pel) {
        this.pel = pel;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String string) {
        super.onPostExecute(string);
        Gson gson = new Gson();
        ArrayList<Auteur> listAuteur = gson.fromJson(
                string,
                new TypeToken<List<Auteur>>(){}.getType());
        pel.onPostExecuteListe(listAuteur);

    }

    @Override
    protected String doInBackground(URL... urls) {
        try{
            return getResponseFromHttpUrl(urls[0]);
        }catch (IOException e){
            e.printStackTrace();
            return "error";
        }

    }



    public static String getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }
    public interface PostExecuteList{
        void  onPostExecuteListe(ArrayList<Auteur> a);
    }
}
