package com.imie.chatorvault;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.imie.chatorvault.API.Message;

import java.util.ArrayList;

/**
 * Created by quentin for Chat on 20/12/2017.
 */

public class MessageViewHolder extends RecyclerView.ViewHolder {
    TextView itemTextViewContenu;
    TextView itemTextViewDate;
    TextView itemTextViewAuteur;
    ArrayList<Message> arrayList;

    public MessageViewHolder(View itemView) {
        super(itemView);
        itemTextViewAuteur = (TextView) itemView.findViewById(R.id.textViewAuteur);
        itemTextViewDate = (TextView) itemView.findViewById(R.id.textViewDate);
        itemTextViewContenu = (TextView) itemView.findViewById(R.id.textViewContenu);
    }

    void bind(int listIndex){
        itemTextViewDate.setText(arrayList.get(listIndex).getDate_publication());
        itemTextViewContenu.setText(arrayList.get(listIndex).getContenu());
        itemTextViewAuteur.setText(arrayList.get(listIndex).getAuteur());
    }
}
