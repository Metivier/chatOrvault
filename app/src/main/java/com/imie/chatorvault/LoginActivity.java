package com.imie.chatorvault;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.imie.chatorvault.API.Auteur;

import java.net.MalformedURLException;
import java.security.Key;
import java.util.ArrayList;
import java.util.Iterator;

public class LoginActivity extends AppCompatActivity {
    private final static String KEY = "Login";
    private final static String PSEUDO_KEY = "pseudo";
    private final static String ID_KEY = "id";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //VUES
        final EditText editTextNom = (EditText) findViewById(R.id.editTextName);
        final EditText editTextPswd = (EditText) findViewById(R.id.editTextPswd);
        Button connexion = findViewById(R.id.buttonConnexion);
        Button inscription = findViewById(R.id.buttonInscription);

        if (getSharedPreferences(KEY, MODE_PRIVATE).getString(PSEUDO_KEY, null) != null){
            Intent in = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(in);
            finish();
        }


        connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AsyncLogin asyncLogin = new AsyncLogin(new AsyncLogin.PostExecuteList() {
                    @Override
                    public void onPostExecuteListe(ArrayList<Auteur> a) {
                        String nom = editTextNom.getText().toString();
                        String pswd = editTextPswd.getText().toString();

                        for (Iterator<Auteur> i = a.iterator(); i.hasNext(); ) {
                            Auteur item = i.next();
                            if (item.getNom().equals(nom)) {
                                if (item.getMdp().equals(pswd)) {
                                    SharedPreferences sp = getApplicationContext().getSharedPreferences(KEY,MODE_PRIVATE);
                                    sp.edit().putString(PSEUDO_KEY,nom).apply();
                                    sp.edit().putInt(ID_KEY,item.getId()).apply();
                                    Intent in = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(in);
                                    finish();
                                    return;
                                }
                            }
                        }
                        Toast.makeText(getApplicationContext(),
                                "Mauvais nom ou mauvais mot de passe",
                                Toast.LENGTH_LONG).show();
                    }
                });
                try {
                    asyncLogin.execute(buildURI());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });
        inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nom = editTextNom.getText().toString();
                String pswd = editTextPswd.getText().toString();
                AsyncSignUp asyncSignUp = new AsyncSignUp(nom, pswd, new AsyncSignUp.PostExecuteSignUp() {
                    @Override
                    public void onPostExecuteSignUp(Auteur auteur) {
                        //TODO AJOUT SHARED PREF
                        SharedPreferences sp = getApplicationContext().getSharedPreferences(KEY,MODE_PRIVATE);
                        sp.edit().putString(PSEUDO_KEY,auteur.getNom()).apply();
                        sp.edit().putInt(ID_KEY,auteur.getId()).apply();
                        Intent in = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(in);
                        finish();

                    }
                });
                try {
                    asyncSignUp.execute(buildURI());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public java.net.URL buildURI() throws MalformedURLException {
        Uri constructionURI = Uri.parse(getString(R.string.url_serveur)).buildUpon()
                .appendEncodedPath("auteur/")
                .build();
        return new java.net.URL(constructionURI.toString());
    }
}
