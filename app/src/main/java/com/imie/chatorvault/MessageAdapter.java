package com.imie.chatorvault;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.imie.chatorvault.API.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by quentin for Chat on 20/12/2017.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageViewHolder>{
    ArrayList<Message> arrayList;
    public void addMessage(Message mess){
        arrayList.add(mess);
        notifyDataSetChanged();
    }
    public void addMessages(ArrayList<Message> messages){
        arrayList.addAll(messages);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {

    }
}
