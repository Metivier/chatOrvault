package com.imie.chatorvault.API;

/**
 * Created by quentin for Chat on 19/12/2017.
 */

public class Message {
    String contenu;
    int auteur;
    String date_publication;
    String groupe_correspondant;

    public Message(String contenu, int auteur) {
        this.contenu = contenu;
        this.auteur = auteur;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }


    public String getDate_publication() {
        return date_publication;
    }

    public void setDate_publication(String date_publication) {
        this.date_publication = date_publication;
    }

    public void setAuteur(int auteur) {
        this.auteur = auteur;
    }

    public void setGroupe_correspondant(String groupe_correspondant) {
        this.groupe_correspondant = groupe_correspondant;
    }

    public int getAuteur() {
        return auteur;
    }

    public String getGroupe_correspondant() {
        return groupe_correspondant;
    }
}
