package com.imie.chatorvault.API;

/**
 * Created by quentin for Chat on 19/12/2017.
 */

public class Auteur {
    private String nom;
    private String mdp;
    private int id;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public Auteur(String nom, String mdp, int id) {
        this.nom = nom;
        this.mdp = mdp;
        this.id = id;
    }
    public Auteur(String nom, String mdp) {
        this.nom = nom;
        this.mdp = mdp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
