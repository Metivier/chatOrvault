package com.imie.chatorvault;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.imie.chatorvault.API.Message;

import java.io.DataOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by quentin for Chat on 19/12/2017.
 */

public class AsyncPostMessage extends AsyncTask<URL,Integer,String> {
    String contenu;
    PostExecuteMessage postExecuteMessage;
    public AsyncPostMessage(String contenu, PostExecuteMessage pesu) {
        this.contenu = contenu;
        this.postExecuteMessage = pesu;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String string) {
        super.onPostExecute(string);
        postExecuteMessage.onPostExecuteMessage(string);

    }

    @Override
    protected String doInBackground(URL... urls) {

        //PARAMETRES
        Message message = new Message(contenu,1);
        String retour = null;

        OutputStream out = null;
        try {
            HttpURLConnection conn = (HttpURLConnection) urls[0].openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept","application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);

            Log.i("JSON", new Gson().toJson(message));
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
            os.writeBytes(new Gson().toJson(message));
            os.close();
            os.flush();
            if (conn.getResponseCode() == 201)
                retour = message.getContenu();

            Log.i("STATUS", String.valueOf(conn.getResponseCode()));
            Log.i("MSG" , conn.getResponseMessage());

            conn.disconnect();
            return retour;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "";
        }
    }

    public interface PostExecuteMessage {
        void onPostExecuteMessage(String contenu);
    }
}
